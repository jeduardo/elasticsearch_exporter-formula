{% from slspath+"/map.jinja" import elasticsearch_exporter with context %}

elasticsearch_exporter-remove-service:
  service.dead:
    - name: elasticsearch_exporter
  file.absent:
    - name: /etc/systemd/system/elasticsearch_exporter.service
    - require:
      - service: elasticsearch_exporter-remove-service
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: elasticsearch_exporter-remove-service

elasticsearch_exporter-remove-symlink:
   file.absent:
    - name: {{ elasticsearch_exporter.bin_dir}}/elasticsearch_exporter
    - require:
      - elasticsearch_exporter-remove-service

elasticsearch_exporter-remove-binary:
   file.absent:
    - name: {{ elasticsearch_exporter.dist_dir}}
    - require:
      - elasticsearch_exporter-remove-symlink

elasticsearch_exporter-remove-env:
    file.absent:
      - name: {{ elasticsearch_exporter.config_dir }}
      - require:
        - elasticsearch_exporter-remove-binary
