{% from slspath+"/map.jinja" import elasticsearch_exporter with context %}

elasticsearch_exporter-create-user:
  user.present:
    - name: {{ elasticsearch_exporter.service_user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

elasticsearch_exporter-create-group:
  group.present:
    - name: {{ elasticsearch_exporter.service_group }}
    - system: True
    - members:
      - {{ elasticsearch_exporter.service_user }}
    - require:
      - user: {{ elasticsearch_exporter.service_user }}

elasticsearch_exporter-bin-dir:
  file.directory:
   - name: {{ elasticsearch_exporter.bin_dir }}
   - makedirs: True

elasticsearch_exporter-dist-dir:
 file.directory:
   - name: {{ elasticsearch_exporter.dist_dir }}
   - makedirs: True

elasticsearch_exporter-install-binary:
 archive.extracted:
   - name: {{ elasticsearch_exporter.dist_dir }}/elasticsearch_exporter-{{ elasticsearch_exporter.version }}
   - source: https://github.com/justwatchcom/elasticsearch_exporter/releases/download/v{{ elasticsearch_exporter.version }}/elasticsearch_exporter-{{ elasticsearch_exporter.version }}.{{ grains['kernel'] | lower }}-{{ elasticsearch_exporter.arch }}.tar.gz
   - skip_verify: True
   - options: --strip-components=1
   - user: {{ elasticsearch_exporter.service_user }}
   - group: {{ elasticsearch_exporter.service_group }}
   - enforce_toplevel: False
   - unless:
     - '[[ -f {{ elasticsearch_exporter.dist_dir }}/elasticsearch_exporter-{{ elasticsearch_exporter.version }}/elasticsearch_exporter ]]'
 file.symlink:
   - name: {{ elasticsearch_exporter.bin_dir }}/elasticsearch_exporter
   - target: {{ elasticsearch_exporter.dist_dir }}/elasticsearch_exporter-{{ elasticsearch_exporter.version }}/elasticsearch_exporter
   - mode: 0755
   - unless:
     - '[[ -f {{ elasticsearch_exporter.bin_dir }}/elasticsearch_exporter ]] && {{ elasticsearch_exporter.bin_dir }}/elasticsearch_exporter -v | grep {{ elasticsearch_exporter.version }}'


#elasticsearch_exporter-symlink-binary:
#  file.symlink:
#    - name: {{ elasticsearch_exporter.bin_dir }}/elasticsearch_exporter
#    - target: {{ elasticsearch_exporter.dist_dir }}/elasticsearch_exporter-{{ elasticsearch_exporter.version }}
#    - mode: 0755
